function [error, error_max] = bvp(N)
%format longEng

i = 1;
h=1./N;
x = [h:h:1-h];

A=(2/h^2)*diag(ones(N-1,1))-(1/h^2)*diag(ones(N-2,1),1)-(1/h^2)*diag(ones(N-2,1),-1);
%b=ones(N-1,1);
b(:,1) = sin(x*pi)*pi*pi 

%exact solution
%fu_exact = @(y) 0.5.*y.*(1.-y);
fu_exact = @(y) sin(y*pi); 
u_exact = fu_exact(x)

% direct solve 
u_calc = inv(A)*b;

%plot numerial vs exact solution
figure(1)
plot(x,u_exact,'b');
hold on;
plot(x,u_calc,'m');

u_diff = transpose(u_calc) - u_exact
% maximum error
error_max = max(abs(u_diff));

%2-error
u_diff = h.*u_diff.*u_diff;
error = sqrt(sum(u_diff))
