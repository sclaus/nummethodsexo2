Nmax =100;

i = 1;
for N=5:Nmax
    
h=1./N;
A=(2/h^2)*diag(ones(N-1,1))-(1/h^2)*diag(ones(N-2,1),1)-(1/h^2)*diag(ones(N-2,1),-1);
b=ones(N-1,1);
D=diag(diag(A));
E = -(tril(A)-D);
F = -(triu(A)-D);
I = eye(N-1);

BJ = I - inv(D)*A;
eigJ = eig(BJ);
lambdaBJ(1,i) = max(abs(eigJ));

BGS = inv(D-E)*F;
eigGS = eig(BGS);
lambdaBGS(1,i) = max(abs(eigGS));

i=i+1;
end


% print out spectral radius
format longEng
lambdaBJ
lambdaBGS

figure(1)
N = [5:Nmax];
plot(N,lambdaBJ,'b');
hold on;
plot(N,lambdaBGS,'g');
legend('Jacobi','Gauss Seidel')
