i =1
N = 10

%compute error for several refinements
for k = 1:3
h(i,1) = 1./N;
% solve problem for N
[error(i,1), error_max(i,1)] = bvp(N);
i = i+1;
N = 2*N;
end 

% print convergence order to screen
p = log2(error(2,1)/error(3,1))

%plot error in log-log graph
fig = figure(2);
loglog(h,error, '-bx');
hold on
loglog(h,error_max, '-ko');
grid on
h2 = h.*h;
loglog(h,h2, '--g')
loglog(h,h, '--mo')

legend('error','error max','2','1')
xlabel('log(h)')
ylabel('log(e)')